---
layout: handbook-page-toc
title: "Entity-Specific Employment Policies"
description: "A directory of employment-related policies categorized by entity"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

This page will serve as a directory for our team members employed through a GitLab entity to locate employment-related policies specific to their country of residence. These policies do not supersede [GitLab Inc.'s Code of Business Conduct & Ethics](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d). 

## GitLab Entities

<details>
<summary markdown="span">GitLab BV (Belgium)</summary>

### Belgium

**Workplace Regulations**
* [GitLab BV (Belgium) Work Regulations - Dutch](https://docs.google.com/document/d/1zG6TEU8bhQuZ8KIQzrZtsLZbdlxZbBA1MlqNowCP3h0/edit)
* [GitLab BV (Belgium) Work Regulations - French](https://docs.google.com/document/d/1quto4ZV1gmQrvWZltDFc7f6eL9-VrHb2ocaK2XM3QZQ/edit)
* [GitLab BV (Belgium) Work Regulations - English](https://docs.google.com/document/d/1crinS71YiKQQOCyMvHpYGRZvH3yuQhwZWtBcN-sUYmM/edit)
</details>

<details>
<summary markdown="span">GitLab BV (Netherlands)</summary>

### Netherlands 

**Health and Safety**
* [Safety Regulations for Remote Work](https://docs.google.com/document/d/1tKqPcjPeEgWdYWol3X8YWhqcbbS6SWrJ/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true)

</details>

<details>
<summary markdown="span">GitLab Canada Corp.</summary>

### Canada

* [Right to Disconnect Policy](https://docs.google.com/document/d/1XaVIy3J1depX8s9yUM7YcgpKm6RSNS7V9CPS_rx_K5I/edit?usp=sharing)

</details>

<details>
<summary markdown="span">GitLab France S.A.S.</summary>

### France 

* [Remote Work Policy](https://docs.google.com/document/d/1_8-m3QVNXIN7idfv-F0eYJqRhipjrsWx/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true)
</details>

<details>
<summary markdown="span">GitLab GmbH (Germany)</summary>

### Germany 

**Health and Safety**

To ensure the health and safety of our team members in Germany, and to maintain compliance with the German Occupational Safety and Health Act, all team members in Germany will complete the following checklist at onboarding. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, please contact [People Connect](mailto:people-connect@gitlab.com). If you need to report an accident or injury, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).

* [Work from Home Checklist](https://docs.google.com/document/d/1Z3i-vrkcU5ald0j-scf2rgTumwFCc_sI/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true)

</details>

<details>
<summary markdown="span">GitLab GK (Japan)</summary>

### Japan

* [Work Rules](https://drive.google.com/file/d/1_xXSlx8e3PX_hM5GBgUPgfAlMhe0DkvM/view?usp=sharing)

</details>

<details>
<summary markdown="span">GitLab Inc. (US)</summary>

### United States

**Health and Safety**
* [Workplace Evaluation](https://docs.google.com/document/d/12o3YXX6fIi2-V7yS_bhc1Vx_5jokQh3Oracc-ZIBsGE/edit?usp=sharing) (for existing team members)
* [Onboarding Checklist](https://docs.google.com/document/d/1DqbNBL6QIFBQlxqi7rWQiodFt6KvSp5fml3llu94Byw/edit?usp=sharing) (for new team members)
</details>

<details>
<summary markdown="span">GitLab Ireland LTD</summary>

### Ireland

**Complaint and Disciplinary Procedures**

* [Bullying, Harassment, and Sexual Harassment Policy](https://docs.google.com/document/d/192-0TKUm1x0EmP_SQCnTDOJlDiT_O-O4n7qFFfhWnyA/edit?usp=sharing)
* [Disciplinary Procedure](https://docs.google.com/document/d/1cTSESXTt1Fn8680AVAcpq1g3wkJ_voFj192ak3rXNcA/edit?usp=sharing)
* [Grievance Procedure](https://docs.google.com/document/d/1RJ5rWMz_FRiKDOOyAgCn5b4XZkV0dcrKZ82D7etBlUo/edit?usp=sharing)
* [Protected Disclosure Policy](https://docs.google.com/document/d/1cIRHYLBCKJy2doHcpL4Yg-A4PWaw16IgjWLUCb34OyE/edit?usp=sharing)
* [Right to Disconnect Policy](https://docs.google.com/document/d/1lEseZcXPUyH8NFBL8cKri7tQ4H47dB84IGARioqB09k/edit)

**Health and Safety**

* [Display Screen Equipment Regulations 2007](https://www.irishstatutebook.ie/eli/2007/si/299/made/en/print#partii-chapv)

* To ensure the physical and mental health and safety of our team members in Ireland, and to maintain compliance with local employment regulations, all team members in Ireland will complete a [home working checklist](https://forms.gle/bmXqNdH1xEw2bFTa8) at onboarding. This checklist will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, please contact [People Connect](mailto:people-connect@gitlab.com). If you need to report an accident or injury, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com). 

</details>

<details>
<summary markdown="span">GitLab Korea LTD</summary>

### Korea

</details>

<details>
<summary markdown="span">GitLab LTD (UK)</summary>

### United Kingdom

**Health and Safety**

[GitLab Ltd Health and Safety Risk Assessment](https://docs.google.com/document/d/1rJmwBW6eP7Db5VF2_GlFBr-DbDQfLbEHkn-IUHzekhM/edit?usp=sharing)

To ensure the physical and mental health and safety of our team members in the UK, and to maintain compliance with local employment regulations, all team members employed by GitLab Ltd. must review the [guidelines for working comfortably at home](https://docs.google.com/document/d/1aFVBgpfLIyuV14kE0kavHsdl5_fWjnnP/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true) and complete a [home working risk assessment](https://docs.google.com/document/d/1hJDuR1wqEcSCq3us2FnsV_b4My7STgctyM-c_FDqfAU/edit?usp=sharing) at onboarding. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, please contact [People Connect](mailto:people-connect@gitlab.com). If you need to report an accident or injury, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com). Team members should regularly inspect their home offices for potential hazards related to the following:
  - Electrical equipment: make sure that your plugs, wiring, and casings are in good working order and there is no fraying.
  - Fire: regularly check your smoke detectors and develop an escape plan in case of fire, if you haven't already.
  - Slips and trips: keep your work area clear of boxes, cables, uneven surfaces, and other obstacles that may cause a trip hazard. 
  - First aid: Keep first aid provisions in your home and be careful when handling sharp objects and hot or cold liquids.

The handbook also has a wealth of information and recommendations for setting up your workspace and procuring the proper equipment: 
  * [How to Create an Ergonomic Workspace](https://about.gitlab.com/company/culture/all-remote/tips/#create-an-ergonomic-workspace)
  * [Focus your Workspace](https://about.gitlab.com/company/culture/all-remote/getting-started/#focus-your-workspace)
  * [Combating Burnout, Isolation, and Anxiety in a Remote Workplace](https://about.gitlab.com/company/culture/all-remote/mental-health/)
  * [Considerations for a Productive Home Office](https://about.gitlab.com/company/culture/all-remote/workspace/#introduction)
  * [Equipment Examples](https://about.gitlab.com/handbook/finance/expenses/#-not-sure-what-to-buy-equipment-examples)
  * [Hardware Expense Guide](https://about.gitlab.com/handbook/finance/expenses/#-hardware)

**Working Time Regulations**

_What are the Working Time Regulations?_

The Working Time Regulations (1998) were introduced to support the health and safety of workers by setting maximum requirements for the number of hours that they can work, paid time off and rest periods.

_The 48-hour working week_

The regulations state it is illegal for you to work any time over a total of 48 hours each week. You can agree to exceed this limit if you want to, but you cannot be mandated to work more than 48 hours per week.
Average working hours are calculated over a ‘reference’ period, which is usually 17 weeks. This means you can work more than 48 hours one week, as long as the average over 17 weeks is less than 48 hours a week.
For more information about the 48-hour working week restrictions, please visit the [government's website](https://www.gov.uk/maximum-weekly-working-hours).

_How to opt-out of the 48-hour working week_

It is entirely your decision if you would like to work more than an average of 48 hours a week. GitLab is committed to enabling team members to maintain a healthy work-life balance, but on occasion (for example during periods of high customer demand) you may be asked to work longer hours. To ensure that we remain compliant with the regulations, we ask you to opt out of them, in case such a need arises. 
If you would like to opt-out, you can do this by signing this written agreement, known as the opt-out agreement. You may have chosen to sign/confirm this agreement already during your onboarding. The duration of the agreement is indefinite. If you haven't already signed [this agreement](https://docs.google.com/document/d/1DRu62fqQW7OQV0lS6evf_XxdQ-0IVZl1Jk_LmSgEhDo/edit#heading=h.tkxekwf0szl9) but would like to do so, you can make a copy of this document, add your name and sign it, and email a copy to people-connect@gitlab.com, with a request that it be saved with your documents in Workday.

_How do I cancel my opt-out agreement?_

You can choose to cancel your opt-out agreement at any time. The notice period you are required to give is three months.

</details>

<details>
<summary markdown="span">GitLab PTY (Australia)</summary>

### Australia 

**Health and Safety**

To ensure the physical and mental health and safety of our team members in Australia, and to maintain compliance with local employment regulations, all team members in Australia will complete the following checklist at onboarding. This checklist will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, please contact [People Connect](mailto:people-connect@gitlab.com). If you need to report an accident or injury, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).

* [Remote Work Checklist](https://docs.google.com/document/d/1_sHk3ksGLDVxBZsnO3pMD-U_R_Fy0Yyu/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true)

</details>

<details>
<summary markdown="span">GitLab PTY (New Zealand)</summary>

### New Zealand

**Health and Safety**

To ensure the physical and mental health and safety of our team members in New Zealand, and to maintain compliance with local employment regulations, all team members in New Zealand will complete the following survey at onboarding. The responses will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, please contact [People Connect](mailto:people-connect@gitlab.com). If you need to report an accident or injury, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).

* [Remote Work Checklist](https://forms.gle/DszZNkBA22Rhy3VW7)
</details>

<details>
<summary markdown="span">GitLab Singapore PTE LTD</summary>

### Singapore

**Health and Safety**

* [How to Create an Ergonomic Workspace](https://about.gitlab.com/company/culture/all-remote/tips/#create-an-ergonomic-workspace)
* [Focus your Workspace](https://about.gitlab.com/company/culture/all-remote/getting-started/#focus-your-workspace)
* [Combating Burnout, Isolation, and Anxiety in a Remote Workplace](https://about.gitlab.com/company/culture/all-remote/mental-health/)
* [Considerations for a Productive Home Office](https://about.gitlab.com/company/culture/all-remote/workspace/#introduction)
* [Equipment Examples](https://about.gitlab.com/handbook/finance/expenses/#-not-sure-what-to-buy-equipment-examples)
* [Hardware Expense Guide](https://about.gitlab.com/handbook/finance/expenses/#-hardware)
 
**Data Protection/Privacy Policy**
* [Employee Privacy Policy](https://about.gitlab.com/handbook/legal/privacy/employee-privacy-policy/)
* [GitLab Privacy Policy](https://about.gitlab.com/privacy/)
 
**Workplace Harassment Policy**
* [Anti-Harassment Policy](https://about.gitlab.com/handbook/anti-harassment/#introduction)
* [Code of Business Conduct & Ethics](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d)
 
**Fair Employment Practices Policy**
* [GitLab PTE LTD Fair Employment Practices Policy](https://docs.google.com/document/d/1osJfO9BysOqjpt5iLnqygXX7B5Imb9NXIXA3KBtDCJk/edit?usp=sharing)
</details> 
