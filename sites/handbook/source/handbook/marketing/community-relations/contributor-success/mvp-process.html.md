---
layout: handbook-page-toc
title: "GitLab MVP Selection Process"
description: Process for Contributor Success to select GitLab MVPs
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

- - -

# GitLab MVP Selection Process

## GitLab MVP

Each release post GitLab recognizes a community contributor as the MVP ("Most Valuable Person") for the release. Beginning with the 15.9 release, the Contributor Success team took over the nomination and selection process for the GitLab MVP. These community contributors are recognized within the GitLab release post and across Slack and Discord. MVPs also receive a GitLab swag pack in celebration of their contribution.

- Hall of fame list of [GitLab MVPs](https://about.gitlab.com/community/mvp/)
- [Release posts](https://about.gitlab.com/releases/categories/releases/) on GitLab blog

## Workflow for selecting GitLab MVP

1. A [nomination issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/release-post-mvp-nominations.md) is automatically generated on 3rd of the month.
1. Link to the nomination issue on Slack channels in `#release-post`, `#community-relations`, `#mr-coaching` and `#core` after issue is created.
1. Encourage team members to vote by sharing reminders in the above Slack channels. 
  - Complete by 12th of the month or earliest business day.
1. Select the MVP in the nomination issue by choosing the [eligible nominee](/handbook/marketing/community-relations/contributor-success/mvp-process.html#mvp-eligibility) with the most votes
  - Votes are cast as :thumbsup: emojis under the nominee thread in the issue. Other emojis are not counted, but if a vote is close please remind voters they must use the :thumbsup: to vote.
  - Complete by 15th of the month or earliest business day.
  - If no MVP nominations have been added to the MVP issue by the 15th or earliest business day, resend reminders to the Slack channels by sharing the original solicitation posts again. Ping the channel and note the lack of nominations and due date. E.g. in #community-relations:
      ```md
      @community-team - We only have one (or none) MVP nomination. I’m going to wait until the end of the day, 12pm UTC for other nominations. Please nominate a community contributor that delivered something great for X-Y!
      ```
1. Use the [MVP write-up issue template](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/mvp-write-up.md) to collaborate with the MVP winner and their nominator to create a write-up blurb for the release post.
  - Complete write-up blurb by 20th of the month
1. Announce the MVP winner in team member Slack channels and community Discord. 
1. Add MVP winner to "hall of fame" [data/mvps.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/mvps.yml) file
  - Add release version, MVP name, user handle, release post date and release post URL
  - Submit by 20th of the month
1. Update the `data/release_posts/x_y/mvp.yml` file with MVP information. E.g.`data/release_posts/15_8/mvp.yml` targeting the `release-15-8` branch.
  - Add MVP name, user handle and write-up blurb
  - Submit by 20th of the month
1. Mention the `@community-team` in the `#community-relations` channel and ask them to send the swag pack to the MVP.
  - Follow up with Community Relations to confirm the MVP has been sent their swag
1. Share a thanks message to team members who participated in the nomination process.
  - Link to the release post MVP section.
  - Ping any team members who added nominations, supportive comments or :thumbsup: votes within the MVP nomination issue.

### MVP Eligibility

- The Contributor Success team will make the final choice on the GitLab MVP within the nomination issue and should not wait for consensus.
- There can only be one MVP per release post.
- A contributor is eligible to be MVP once per major release cycle. For example, if they are MVP during any 13.* milestone, they cannot be an MVP again until the 14.0 milestone.
- A quick way to check past MVPs is to view [`/data/mvps.yaml`.](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/mvps.yml)

### MVP Write-Up Blurb

Use the [MVP write-up issue template](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/mvp-write-up.md) to collaborate on the release post blurb.

The MVP write-up section should:
- Contain a brief description of the MVP's release contribution and summary of prior GitLab contributions.
- A link to the MVP's GitLab profile.
- Any links to relevant issues, MRs, issue boards or epics the MVP contributed to.
- Contributor Success is responsible for reviewing the entry for:
  - Consistency and accuracy
  - Correct and workings links for user information, issues, MRs, etc.
  - Correct spelling of names, organizations, product features, etc.
  - Correct [prounoun](https://about.gitlab.com/handbook/people-group/pronouns/) usage
- The write-up should be submitted by the 20th of the month to the `data/release_posts/x_y/mvp.yml` file targeting the specific release branch
