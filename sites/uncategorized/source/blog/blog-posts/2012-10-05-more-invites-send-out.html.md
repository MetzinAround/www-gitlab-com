---
title: More invites send out
description: "Today we invited 100 more people to create an account, I'm glad to get more interested people on Gitlab.com!"
canonical_path: "/blog/2012/10/05/more-invites-send-out/"
tags: untagged
categories: company
status: publish
type: post
published: true
meta:
  _wpas_done_1532919: '1'
  publicize_reach: a:2:{s:7:"twitter";a:1:{i:1532919;i:33;}s:2:"wp";a:1:{i:0;i:1;}}
  _elasticsearch_indexed_on: '2012-10-05 10:41:57'
---
Today we invited 100 more people to create an account, I'm glad to get more interested people on Gitlab.com! You can [sign up](https://gitlab.com/users/sign_up) if you want to join as well.